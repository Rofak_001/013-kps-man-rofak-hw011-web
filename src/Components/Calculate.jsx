import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button,Card,Form,ListGroup} from 'react-bootstrap';
import ImageCal from '../Image/Calculate.png';
import Result from './Result';
export default class Calculate extends Component {
    constructor(props){
        super(props);
        this.state={
            arrNumber:[]
        }
    }
    myCal=()=>{
        var myOption=document.getElementById("option");
        var number1=document.getElementById("number1");
        var number2=document.getElementById("number2");
        var rs;
        if(isNaN(number1.value) || isNaN(number2.value)){
            alert("Cannot Calculate!");
        }else if(number1.value==="" || number2.value===""){
            alert("Cannot Calculate!");
        }else{
            if(myOption.value==="Plus"){
                rs=parseInt(number1.value)+parseInt(number2.value);            
            }else if(myOption.value==="Subtract"){
                rs=number1.value-number2.value;
            }else if(myOption.value==="Multiply"){
                rs=number1.value * number2.value;
            }else if(myOption.value==="Divide"){
                rs=number1.value / number2.value;
            }else if(myOption.value==="Module"){
                rs=number1.value % number2.value;
            }
            this.state.arrNumber.push(rs); 
        }
            console.log(rs);
        // this.setState({history:rs});
        // if(this.state.history!==""){      
            console.log(this.state.arrNumber)       
        // } 
        // after click will render 
        this.forceUpdate();  
    }
    render() {
        var list=this.state.arrNumber.map((value,index)=><Result key={index} history={value}/>)
        return (
            <div className="row">  
                <div className="col-4">
                    <Card>
                    <Card.Img variant="top" src={ImageCal} style={{ width: '100%' }} />
                    <Card.Body>
                        <Form.Group>
                            <Form.Control type="Text" id="number1" placeholder="Input Number1"/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Control type="Text" id="number2" placeholder="Input Number2"/>
                        </Form.Group>
                        <Form.Group>             
                            <Form.Control as="select"id="option">
                                <option value="Plus"> + Plus</option>
                                <option value="Subtract"> - Subtract</option>
                                <option value="Multiply"> * Multiply</option>
                                <option value="Divide"> / Divide</option>
                                <option value="Module"> % Module</option>
                                </Form.Control>
                            </Form.Group>
                            <Button variant="primary" onClick={()=>this.myCal()}>Calculator</Button>
                        </Card.Body>
                        </Card>
                    </div>
                <div className="col-4">
                    <h4>Result History</h4>
                    <ListGroup>
                        {list}
                    </ListGroup>   
                </div>
            </div>
        );
    }
}