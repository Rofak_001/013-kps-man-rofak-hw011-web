import React from 'react';
import ReactDOM from 'react-dom';
import Calculate from './Components/Calculate'
class App extends React.Component {
   render() {
      return (
         <div className="container mt-4">                  
            <Calculate/>               
         </div>    
      );
   }
}

ReactDOM.render(<App />, document.getElementById('root'));